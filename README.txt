UI Patterns Block
This module provides a block plugin which exposes UI Patterns previews.

1) Install the module as normal.
2) Have at least one UI Pattern defined with a preview.
3) Select "Place block" in the block UI and select the UI Patterns Block.
4) Choose the desired UI Pattern and save.
